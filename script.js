// Asign Elements
const form = document.querySelector("#form");
const formInput = document.querySelector("#form-input");
const taskList = document.querySelector(".list");


// Functions
function addTask(e) {
    e.preventDefault();

    // Create new Task
    const newTask = document.createElement("div");
    newTask.classList.add("task");
    taskList.appendChild(newTask);

    // Create new Task Text
    const newTaskText = document.createElement("span");
    newTaskText.innerText = formInput.value;
    newTaskText.classList.add("task__text");
    newTask.appendChild(newTaskText);

    // Create new Remove Task
    const newTaskRemove = document.createElement("span")
    newTaskRemove.innerHTML = "&times;";
    newTaskRemove.classList.add("task__remove");
    newTask.appendChild(newTaskRemove);

    // Add Event Listeners
    newTaskText.addEventListener("click", function(e) {
        e.target.classList.toggle("stroked")
    }, false);
    newTaskRemove.addEventListener("click", function(e) {
        e.target.parentElement.classList.add("removed");
        setTimeout(() => {
            e.target.parentElement.remove()
        }, 100)
    }, false);

    formInput.value = "";
}

// Event Listeners
form.addEventListener("submit", addTask, false);